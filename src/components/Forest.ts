
export enum State {
    Normal = 'green',
    Feu = 'red',
    Cendre = 'grey'
  }
  
  interface ForestModel {
    coordX: number;
    coordY: number;
    etat: State;
  }
  
  export default class Forest implements ForestModel {
    coordX!: number;
    coordY!: number;
    etat!: State;
  
    constructor(X: number, Y: number, etat: State = State.Normal) {
      this.coordX = X;
      this.coordY = Y;
      this.etat = etat;
    }
  
    turnToFire(): void {
      this.etat = State.Feu;
    }
  
    turnToAsh(): void {
      this.etat = State.Cendre;
    }
  
    isOnFire(): boolean {
      return this.etat === State.Feu;
    }

    isOnAsh(): boolean {
      return this.etat === State.Cendre;
    }

    isNormal(): boolean {
      return this.etat === State.Normal;
    }
  }
  